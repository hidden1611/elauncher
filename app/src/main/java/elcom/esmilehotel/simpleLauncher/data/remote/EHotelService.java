package elcom.esmilehotel.simpleLauncher.data.remote;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import elcom.esmilehotel.simpleLauncher.data.local.PreferencesHelper;
import elcom.esmilehotel.simpleLauncher.data.model.App;
import elcom.esmilehotel.simpleLauncher.data.model.ImageBg;
import elcom.esmilehotel.simpleLauncher.injection.ApplicationContext;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx.Observable;
import rx.Subscriber;
import timber.log.Timber;

/**
 * Created by Ann on 3/2/16.
 */
@Singleton
public class EHotelService {

    final OkHttpClient client;
    private final Context context;
    private final PreferencesHelper preferencesHelper;

    @Inject
    public EHotelService(@ApplicationContext Context context,  PreferencesHelper preferencesHelper){
        this.context = context;
        this.preferencesHelper = preferencesHelper;
        client = new OkHttpClient();
        EHotelApi.host = EHotelApi.ENDPOINT;
        EHotelApi.token = "ebop";
    }

    String get(String url) throws IOException {
        Request request = new Request.Builder()
//                .addHeader("Content-Type","text/json;Charset=UTF-8")
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public Observable<String> getData(){
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {


            }
        });
    }



    public Observable<List<App>> getInstalledAppsExcludeApps(List<App> listStaticApp) {
        return Observable.create(subscriber -> {

            try {
                final PackageManager pm = context.getPackageManager();
                List<App> listApp = new ArrayList<>();
                listApp.addAll(listStaticApp);
                List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

                for (ApplicationInfo packageInfo : packages) {

                    try {
                        if(pm.getLaunchIntentForPackage(packageInfo.packageName) == null)
                            continue;

                        boolean isStatic = false;
                        for (App app : listStaticApp) {
                            if(app.getPkg().equals(packageInfo.packageName)){
                                isStatic = true;
                            }
                        }

                        if(!isStatic){
                            listApp.add(new App(packageInfo.loadLabel(pm).toString(), packageInfo.packageName, packageInfo.loadIcon(pm)));
                        }
                    } catch (Exception e) {
                        Timber.d(e, "getAppFromServer");
                    }
                }

                subscriber.onNext(listApp);
                subscriber.onCompleted();

            } catch (Exception e) {
                Timber.d(e, "getInstalledAppsExcludeApps");
                subscriber.onError(e);
            }

        });
    }


    public Observable<List<ImageBg>> backgrounds(String path) {
        return Observable.create(new Observable.OnSubscribe<List<ImageBg>>() {

            @Override
            public void call(Subscriber<? super List<ImageBg>> subscriber) {

                try {

                    List<ImageBg> listImageBg = new ArrayList<>();

                    File f = new File(path);
                    File file[] = f.listFiles();
                    for (File file1 : file) {
                        listImageBg.add(new ImageBg(file1.getName(), file1.getAbsolutePath()));
                    }

                    subscriber.onNext(listImageBg);
                    subscriber.onCompleted();


                } catch (Exception e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                }

            }
        });
    }




}
