package elcom.esmilehotel.simpleLauncher.data.local;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import elcom.esmilehotel.simpleLauncher.data.model.App;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

@Singleton
public class DatabaseHelper {

    private final BriteDatabase mDb;

    @Inject
    public DatabaseHelper(DbOpenHelper dbOpenHelper) {

        mDb = SqlBrite.create().wrapDatabaseHelper(dbOpenHelper, Schedulers.io());
    }

    public BriteDatabase getBriteDb() {
        return mDb;
    }

    /**
     * Remove all the data from all the tables in the database.
     */
    public Observable<Void> clearTables() {
        return Observable.create(subscriber -> {
            if (subscriber.isUnsubscribed()) return;
            BriteDatabase.Transaction transaction = mDb.newTransaction();
            try {
                Cursor cursor = mDb.query("SELECT name FROM sqlite_master WHERE type='table'");
                while (cursor.moveToNext()) {
                    mDb.delete(cursor.getString(cursor.getColumnIndex("name")), null);
                }
                cursor.close();
                transaction.markSuccessful();
                subscriber.onCompleted();
            } finally {
                transaction.end();
            }
        });
    }





    public Observable<App> setStaticApps(final List<App> apps) {
        return Observable.create(subscriber -> {

            if (subscriber.isUnsubscribed()) return;
            BriteDatabase.Transaction transaction = mDb.newTransaction();

            mDb.delete(Db.AppStaticTable.TABLE_NAME, null);

            try {
                for (App app : apps) {
                    long result = mDb.insert(Db.AppStaticTable.TABLE_NAME,
                            Db.AppStaticTable.toContentValues(app),
                            SQLiteDatabase.CONFLICT_REPLACE);

                    if (result >= 0) subscriber.onNext(app);
                }
                transaction.markSuccessful();
                subscriber.onCompleted();
            } finally {
                transaction.end();
            }
        });
    }

    public Observable<App> updateApps(final List<App> apps){
        return Observable.create(subscriber -> {
            if (subscriber.isUnsubscribed()) return;
            BriteDatabase.Transaction transaction = mDb.newTransaction();
            try {
                for (App app : apps) {

                    long result = mDb.update(Db.AppStaticTable.TABLE_NAME,
                            Db.AppStaticTable.toContentValues(app),
                            SQLiteDatabase.CONFLICT_REPLACE,
                            Db.AppStaticTable.COLUMN_App_PACKAGE + " = ?",
                            app.getPkg());
                    if (result >= 0) subscriber.onNext(app);


                }
                transaction.markSuccessful();
                subscriber.onCompleted();
            } finally {
                transaction.end();

            }

        });

    }



    /**
     * 1. Delete App
     * 2. Add New Add
     * 3. Update App
     * @param apps
     * @return
     */
    public Observable<App> setApps(final List<App> apps) {
        return Observable.create(subscriber -> {
            if (subscriber.isUnsubscribed()) return;
            BriteDatabase.Transaction transaction = mDb.newTransaction();

            //Check To delete App
            try {
                Cursor cursor = mDb.query("SELECT * FROM " + Db.AppStaticTable.TABLE_NAME);

                while (cursor.moveToNext()){
                    App app = Db.AppStaticTable.parseCursor(cursor);

                    boolean isDeleteApp = true;
                    for (App app1 : apps) {
                        if(app1.getPkg().equals(app.getPkg())){
                            isDeleteApp = false;
                        }
                    }

                    if(isDeleteApp){
                        mDb.delete(Db.AppStaticTable.TABLE_NAME, Db.AppStaticTable.COLUMN_App_PACKAGE + " LIKE '%"+ app.getPkg() +"%'", null);
                    }

                }


            }catch (Exception e){
                e.printStackTrace();
            }

            try {
                for (App app : apps) {
                    App savedApp =  null;
                    try {
                        String rawQuery = "SELECT * FROM " + Db.AppStaticTable.TABLE_NAME + " WHERE " + Db.AppStaticTable.COLUMN_App_PACKAGE + "=?";
                        Cursor cursor = mDb.query( rawQuery, app.getPkg());
                        cursor.moveToNext();
                        if(cursor.getCount() > 0) {
                            savedApp = Db.AppStaticTable.parseCursor(cursor);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if(savedApp == null){
                        long result = mDb.insert(Db.AppStaticTable.TABLE_NAME,
                                Db.AppStaticTable.toContentValues(app),
                                SQLiteDatabase.CONFLICT_REPLACE);
                        if (result >= 0 ) subscriber.onNext(app);
                    }else if(!savedApp.equals(app)){
                        long result = mDb.update(Db.AppStaticTable.TABLE_NAME,
                                Db.AppStaticTable.toContentValues(app),
                                SQLiteDatabase.CONFLICT_REPLACE,
                                Db.AppStaticTable.COLUMN_App_PACKAGE + " = ?",
                                app.getPkg());
                        if (result >= 0 ) subscriber.onNext(app);
                    }
                }
                transaction.markSuccessful();
                subscriber.onCompleted();
            } finally {
                transaction.end();
            }
        });
    }


    public Observable<List<App>> getAllApps() {
        return mDb.createQuery(Db.AppStaticTable.TABLE_NAME,
                "SELECT * FROM " + Db.AppStaticTable.TABLE_NAME +  " ORDER BY " + Db.AppStaticTable.COLUMN_App_NAME + " ASC")
                .mapToList(cursor -> Db.AppStaticTable.parseCursor(cursor));

    }

    public Observable<List<App>> getShowedApps() {
        return mDb.createQuery(Db.AppStaticTable.TABLE_NAME,
                "SELECT * FROM " + Db.AppStaticTable.TABLE_NAME + " WHERE " + Db.AppStaticTable.COLUMN_App_ISPUBLIC +" <> -1" +  " ORDER BY " + Db.AppStaticTable.COLUMN_App_NAME + " ASC")
                .mapToList(cursor -> Db.AppStaticTable.parseCursor(cursor));

    }

    public Observable<List<App>> getNoShowedApps() {
        return mDb.createQuery(Db.AppStaticTable.TABLE_NAME,
                "SELECT * FROM " + Db.AppStaticTable.TABLE_NAME + " WHERE " + Db.AppStaticTable.COLUMN_App_ISPUBLIC +" = -1" +  " ORDER BY " + Db.AppStaticTable.COLUMN_App_NAME + " ASC"
        )
                .mapToList(cursor -> Db.AppStaticTable.parseCursor(cursor));

    }



}
