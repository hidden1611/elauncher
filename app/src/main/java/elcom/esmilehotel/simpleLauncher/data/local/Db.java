package elcom.esmilehotel.simpleLauncher.data.local;

import android.content.ContentValues;
import android.database.Cursor;

import elcom.esmilehotel.simpleLauncher.data.model.App;


public class Db {

    public Db() {
    }


    public abstract static class AppStaticTable {
        public static final String TABLE_NAME = "AppStatic";
        public static final String COLUMN_App_NAME = "Appname";
        public static final String COLUMN_App_SUBTITLE = "Appsubtitle";
        public static final String COLUMN_App_PACKAGE = "AppPakage";
        public static final String COLUMN_App_ISPUBLIC = "AppIsPublic";

        public static final String CREATE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        COLUMN_App_NAME + " TEXT , " +
                        COLUMN_App_ISPUBLIC + " INTEGER , " +
                        COLUMN_App_SUBTITLE + " TEXT , " +
                        COLUMN_App_PACKAGE + " TEXT " +
                        " ); ";

        public static ContentValues toContentValues(App tvDetail) {

            ContentValues values = new ContentValues();
            values.put(COLUMN_App_NAME, tvDetail.getName());
            values.put(COLUMN_App_SUBTITLE, tvDetail.getSubtle());
            values.put(COLUMN_App_PACKAGE, tvDetail.getPkg());
            values.put(COLUMN_App_ISPUBLIC, tvDetail.getIsPublic());
            return values;

        }

        public static App parseCursor(Cursor cursor) {
            App tvDetail = new App();

            try {
                tvDetail.setName(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_App_NAME)));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            try {
                tvDetail.setSubtle(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_App_SUBTITLE)));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            tvDetail.setPkg(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_App_PACKAGE)));
            tvDetail.setIsPublic(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_App_ISPUBLIC)));
            return tvDetail;
        }
    }
}
