package elcom.esmilehotel.simpleLauncher.data.model;

/**
 * Created by Ann on 4/19/17.
 */

public class ImageBg {

    String name;
    String url;

    public ImageBg(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
