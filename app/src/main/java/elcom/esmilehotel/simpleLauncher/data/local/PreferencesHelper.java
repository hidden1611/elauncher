package elcom.esmilehotel.simpleLauncher.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import elcom.esmilehotel.simpleLauncher.injection.ApplicationContext;


@Singleton
public class PreferencesHelper {

    public static final String PREF_FILE_NAME = "esmilehotel_pref";
    public static final String PREF_BACKGROUND = "PREF_BACKGROUND";

    private final SharedPreferences mPref;

    @Inject
    public PreferencesHelper(@ApplicationContext Context context) {
        mPref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void clear() {
        mPref.edit().clear().apply();
    }
    public void setValue(String key, String value){
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString(key,value);
        editor.commit();
    }

    public void clearValue(String key){
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString(key,null);
        editor.commit();

    }
    public String getValue(String key, String stringdefault){
        return mPref.getString(key,stringdefault);
    }

    public void setValue(String key, boolean value){
        SharedPreferences.Editor editor = mPref.edit();
        editor.putBoolean(key,value);
        editor.commit();
    }

    public boolean getValue(String key, boolean stringdefault){
        return mPref.getBoolean(key,stringdefault);
    }





}
