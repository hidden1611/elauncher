package elcom.esmilehotel.simpleLauncher.data.model;

import android.graphics.drawable.Drawable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ann on 9/20/16.
 */

public class App {

    String name;
    String subtle;
    String pkg;

    int isPublic = -1;
    Drawable drawable;


    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public App(String name, String packageName, Drawable drawable) {

        this.name = name;
        this.pkg = packageName;
        this.drawable = drawable;
        isPublic = -1;

    }

    public int getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(int isPublic) {
        this.isPublic = isPublic;
    }

    public App(String name, String subtle, String pkg, int isPublic) {
        this.name = name;
        this.subtle = subtle;
        this.pkg = pkg;
        this.isPublic = isPublic;
    }

    public App() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubtle() {
        return subtle;
    }

    public void setSubtle(String subtle) {
        this.subtle = subtle;
    }

    public String getPkg() {
        return pkg;
    }

    public void setPkg(String pkg) {
        this.pkg = pkg;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        App app = (App) o;

        if (name != null ? !name.equals(app.name) : app.name != null) return false;
        if (subtle != null ? !subtle.equals(app.subtle) : app.subtle != null) return false;
        return pkg != null ? pkg.equals(app.pkg) : app.pkg == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (subtle != null ? subtle.hashCode() : 0);
        result = 31 * result + (pkg != null ? pkg.hashCode() : 0);
        return result;
    }
}
