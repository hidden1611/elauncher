package elcom.esmilehotel.simpleLauncher.data;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import elcom.esmilehotel.simpleLauncher.data.local.DatabaseHelper;
import elcom.esmilehotel.simpleLauncher.data.model.App;
import elcom.esmilehotel.simpleLauncher.data.model.ImageBg;
import elcom.esmilehotel.simpleLauncher.data.remote.EHotelService;
import elcom.esmilehotel.simpleLauncher.util.RetryWithDelay;
import rx.Observable;

/**
 * Created by Ann on 3/2/16.
 */
@Singleton
public class DataManager {

    private final EHotelService eHotelService;
    private final DatabaseHelper databaseHelper;

    @Inject
    public DataManager(EHotelService eHotelService, DatabaseHelper databaseHelper){

        this.eHotelService = eHotelService;


        this.databaseHelper = databaseHelper;
    }

    public Observable<String> getData(){
        return eHotelService.getData();
    }



    public Observable<App> syncApp(){

        return eHotelService.getInstalledAppsExcludeApps(Arrays.asList())
                .concatMap(apps -> databaseHelper.setApps(apps));

    }
    public Observable<List<App>> allApps(){
        return databaseHelper.getAllApps();
    }

    public Observable<List<App>> showedApp(){
        return databaseHelper.getShowedApps();
    }

    public Observable<List<App>> noShowedApp(){
        return databaseHelper.getNoShowedApps();
    }


    public Observable<App> updateApps(List<App> apps) {
        return databaseHelper.updateApps(apps);
    }

    public Observable<List<ImageBg>> backgrounds(){
        return eHotelService.backgrounds("/mnt/sdcard/ehotel/");
    }
}
