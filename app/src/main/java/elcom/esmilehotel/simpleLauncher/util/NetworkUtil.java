package elcom.esmilehotel.simpleLauncher.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;


public class NetworkUtil {

    /**
     * Returns true if the Throwable is an instance of RetrofitError with an
     * http status code equals to the given one.
     */
    public static boolean isHttpStatusCode(Throwable throwable, int statusCode) {
        return throwable instanceof HttpException
                && ((HttpException) throwable).code() == statusCode;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    /*
        int state = NetworkUtil.getTypeNetworkConnected(this);

        if(state == 0){

        }else if((state & 0x000F) == 2) {

        }else if(((state >> 4 )& 0x000F) == 1) {

        }else {

        }
     */

    /**
     *
     * @param context
     * @return type of network connected:
     * byte 1: type of network
     * 0: disconnect
     * 1: wifi
     * 2: ethernet
     * byte 0: level of wifi
     *
     */

    public static int getTypeNetworkConnected(Context context){

        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = manager.getActiveNetworkInfo();
        if(ni != null  && ni.getState() == NetworkInfo.State.CONNECTED) {
            if(ni.getType() == ConnectivityManager.TYPE_ETHERNET) {
                return 2 << 4;
            }else if(ni.getType() == ConnectivityManager.TYPE_MOBILE){
                return 2 << 4;
            }else if(ni.getType() == ConnectivityManager.TYPE_WIFI){
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                wifiInfo.getRssi();
                int level = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), 5);

                return (1 << 4) + level;
            }
        }
        return 0;

    }




    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0") && !nif.getName().equalsIgnoreCase("eth0")) {
                    continue;
                }

                byte[] macBytes = nif.getHardwareAddress();

                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "02:00:00:00:00:00";
    }

}