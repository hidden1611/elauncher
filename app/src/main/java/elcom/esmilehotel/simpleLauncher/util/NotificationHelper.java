package elcom.esmilehotel.simpleLauncher.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import elcom.esmilehotel.simpleLauncher.R;
import elcom.esmilehotel.simpleLauncher.ui.home.HomeActivity;

/**
 * Created by nna on 21/06/2016.
 */
public class NotificationHelper {

    private final NotificationManager mNotificationManager;
    private final NotificationCompat.Builder builder;
    private Context context;

    public NotificationHelper(Context context){

        this.context = context;
        mNotificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        builder = new NotificationCompat.Builder(context).setSmallIcon(R.mipmap.ic_launcher);
        builder.setDefaults(Notification.DEFAULT_SOUND);

    }


    public Notification getNotify(String ss){
        builder.setContentText(ss);
        builder.setContentTitle("eSmileHotel");
        Intent myIntent1 = new Intent(context, HomeActivity.class);
        myIntent1.putExtra("NOTIFY", true);
        PendingIntent pi = PendingIntent.getActivity(context, 0, myIntent1, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pi);
        builder.setAutoCancel(true);
        return builder.build();
    }

    public void show(String ss){
        mNotificationManager.notify(1, getNotify(ss));
    }
}
