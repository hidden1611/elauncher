package elcom.esmilehotel.simpleLauncher.util;

import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.functions.Func1;

public class RetryWithDelay implements
        Func1<Observable<? extends Throwable>, Observable<?>> {
    private static final String TAG = "RetryWithDelay";

    private static final int DEFAULT_RETRY_COUNT = 5;
    private static final int DEFAULT_RETRY_DELAY = 1000 * 60;

    private final int maxRetries;
    private final int retryDelayMillis;
    private int retryCount;

    public RetryWithDelay() {
        this.maxRetries = DEFAULT_RETRY_COUNT;
        this.retryDelayMillis = DEFAULT_RETRY_DELAY;
        this.retryCount = 0;
    }

    public RetryWithDelay(final int maxRetries, final int retryDelayMillis) {
        this.maxRetries = maxRetries;
        this.retryDelayMillis = retryDelayMillis;
        this.retryCount = 0;
    }

    @Override
    public Observable<?> call(Observable<? extends Throwable> attempts) {
        return attempts.flatMap((Func1<Throwable, Observable<?>>) throwable -> {
            System.out.println("Retry " + retryCount);
            if (throwable instanceof HttpException) {
                Log.d(TAG, "Caught http exception.");
            }

            if (throwable instanceof IOException) {
                Log.d(TAG, "Network error");
            }

            if (++retryCount < maxRetries) {
                // When this Observable calls onNext, the original
                // Observable will be retried (i.e. re-subscribed).
                return Observable.timer(retryDelayMillis, TimeUnit.MILLISECONDS);
            }

            // Max retries hit. Just pass the error along.
            return Observable.error(throwable);
        });
    }
}