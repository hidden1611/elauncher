package elcom.esmilehotel.simpleLauncher.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;

import com.squareup.otto.Bus;

import javax.inject.Inject;

import elcom.esmilehotel.simpleLauncher.SupervisorApplication;
import elcom.esmilehotel.simpleLauncher.data.DataManager;
import elcom.esmilehotel.simpleLauncher.util.EventPosterHelper;
import elcom.esmilehotel.simpleLauncher.util.NotificationHelper;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by nna on 21/06/2016.
 * Authenticate
 * After It get success, it send the event bus to HomePresenter to begin get data.
 * Unless, It will retry and send Event Error.
 */
public class KeepSessionService extends Service {



    private static final int MSG_KEEP_SESSION = 1;
    private static final int TIMEOUT_KEEP_SESSION = 5000;

    int retry;
    boolean isRunning;

    @Inject
    DataManager dataManager;
    @Inject
    EventPosterHelper eventPosterHelper;

    @Inject
    Bus bus;

    Handler mHandler = new Handler(Looper.getMainLooper(), message -> {
        switch (message.what){
            case MSG_KEEP_SESSION:
                keepSession();
                break;
        }
        return false;
    });

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SupervisorApplication.get(this).getComponent().inject(this);
        bus.register(this);
        isRunning = true;
        mHandler.removeCallbacksAndMessages(null);
        mHandler.sendEmptyMessageDelayed(MSG_KEEP_SESSION, TIMEOUT_KEEP_SESSION);

//        NotificationHelper notificationHelper = new NotificationHelper(this);
//        startForeground(1, notificationHelper.getNotify("Running..."));

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        retry = 0;
        authenticateCMS();
        return START_STICKY;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
        isRunning = false;
        stopForeground(true);
        mHandler.removeCallbacksAndMessages(null);

    }

    private void authenticateCMS(){

    }

    private void keepSession(){

    }

    private void login(){

    }

}

