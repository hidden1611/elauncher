package elcom.esmilehotel.simpleLauncher;

import android.app.Application;
import android.content.Context;

import com.squareup.otto.Bus;

import javax.inject.Inject;

import elcom.esmilehotel.simpleLauncher.injection.component.ApplicationComponent;
import elcom.esmilehotel.simpleLauncher.injection.component.DaggerApplicationComponent;
import elcom.esmilehotel.simpleLauncher.injection.module.ApplicationModule;

/**
 * Created by Ann on 2/12/16.
 */
public class SupervisorApplication extends Application {

    @Inject Bus mEventBus;
    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);
        mEventBus.register(this);

    }

    public static SupervisorApplication get(Context context) {
        return (SupervisorApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        this.applicationComponent = applicationComponent;
    }
}
