package elcom.esmilehotel.simpleLauncher.ui.showapp;

import android.animation.AnimatorInflater;
import android.animation.ValueAnimator;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import elcom.esmilehotel.simpleLauncher.R;
import elcom.esmilehotel.simpleLauncher.data.model.App;
import elcom.esmilehotel.simpleLauncher.ui.base.BaseActivity;
import elcom.esmilehotel.simpleLauncher.ui.home.HomeActivity;


public class ShowAppFragment extends Fragment implements ShowAppMvpView {

    @Inject
    ShowAppPresenter mShowAppPresenter;
    @BindView(R.id.rv_list_allapp)
    RecyclerView rv_list;

    AdapterShowApp adapterShowApp;
    private ValueAnimator mBgFadeInAnimator, mBgFadeOutAnimator;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_showapp, container, false);
        ButterKnife.bind(this, fragmentView);

        initRv();
//
//        ValueAnimator.AnimatorUpdateListener listener = new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator valueAnimator) {
//                setBgAlpha(valueAnimator.getAnimatedValue());
//            }
//        };
//        mBgFadeInAnimator = loadAnimator(getActivity(), R.animator.lb_playback_bg_fade_out);
//        mBgFadeInAnimator.addUpdateListener(listener);
//        mBgFadeInAnimator.addListener(mFaceListener);
//
//

        mShowAppPresenter.attachView(this);
        mShowAppPresenter.getApp();
        ((HomeActivity) getActivity()).startSyncService();


        return fragmentView;
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mShowAppPresenter.updateApps(adapterShowApp.getItems());
        mShowAppPresenter.detachView();
    }

    public void initRv(){
        rv_list.setLayoutManager(new GridLayoutManager(getActivity(), 6));
        adapterShowApp = new AdapterShowApp();
        rv_list.setAdapter(adapterShowApp);

        adapterShowApp.setOnItemClickListener(new AdapterShowApp.OnItemClickListener() {
            @Override
            public void OnClick(View view, int position) {

                App app = adapterShowApp.getItem(position);
                app.setIsPublic(app.getIsPublic() == -1 ? 0: -1);
                adapterShowApp.notifyItemChanged(position);

            }
        });

    }

    private static final String TAG = "ShowAppFragment";
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        Log.d(TAG, "onHiddenChanged ");
        if(hidden){
            mShowAppPresenter.updateApps(adapterShowApp.getItems());
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void updateData(List<App> data) {

        adapterShowApp.addItem2(data);

        rv_list.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(rv_list.getLayoutManager().getChildCount() > 0) {
                    rv_list.getLayoutManager().getChildAt(0).requestFocus();
                }

            }
        }, 200);

    }

    private ValueAnimator loadAnimator(Context context, int resId){
        ValueAnimator valueAnimator = (ValueAnimator) AnimatorInflater.loadAnimator(context, resId);
        valueAnimator.setDuration(valueAnimator.getDuration() * 1);
        return valueAnimator;
    }
    public float getY(){

        int heightt = getActivity().getWindowManager().getDefaultDisplay().getHeight();
        return getView().getY() / heightt;
    }
    public void setY(float yFraction){

        int heightt = getActivity().getWindowManager().getDefaultDisplay().getHeight();
        getView().setY(heightt*yFraction);

    }
}