package elcom.esmilehotel.simpleLauncher.ui.setting;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import elcom.esmilehotel.simpleLauncher.R;
import elcom.esmilehotel.simpleLauncher.data.BusEvent;
import elcom.esmilehotel.simpleLauncher.data.model.ImageBg;
import elcom.esmilehotel.simpleLauncher.ui.base.BaseActivity;
import elcom.esmilehotel.simpleLauncher.util.EventPosterHelper;

public class SettingFragment extends Fragment implements SettingMvpView {

    @Inject
    SettingPresenter mSettingPresenter;
    @Inject
    EventPosterHelper eventPosterHelper;

    @BindView(R.id.rv)
    RecyclerView rv;

    BackgroundAdapter backgroundAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_setting, container, false);
        ButterKnife.bind(this, view);
        mSettingPresenter.attachView(this);

        initRv();
        return view;
    }

    private void initRv() {
        rv.setLayoutManager(new GridLayoutManager(getActivity(), 4));

        backgroundAdapter = new BackgroundAdapter();
        backgroundAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {

                ImageBg imageBg = backgroundAdapter.getItem(position);
                eventPosterHelper.postEventSafely(new BusEvent.EventBackground(imageBg.getUrl()));

            }
        });

        rv.setAdapter(backgroundAdapter);



    }


    @Override
    public void onDestroyView() {

        super.onDestroyView();
        mSettingPresenter.detachView();

    }

    @Override
    public void showProcess(boolean show) {

    }

    @Override
    public void updateData(List<ImageBg> data) {
        backgroundAdapter.setNewData(data);
        rv.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(rv.getLayoutManager().getChildCount() > 0) {
                    rv.getLayoutManager().getChildAt(0).requestFocus();
                }

            }
        }, 200);

    }
}
