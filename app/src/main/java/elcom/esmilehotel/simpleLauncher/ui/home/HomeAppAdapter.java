package elcom.esmilehotel.simpleLauncher.ui.home;

import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;

import java.util.List;

import elcom.esmilehotel.simpleLauncher.R;
import elcom.esmilehotel.simpleLauncher.data.model.App;

/**
 * Created by Ann on 4/18/17.
 */

public class HomeAppAdapter extends BaseQuickAdapter< App, BaseViewHolder>  {

    public HomeAppAdapter() {
        super(R.layout.item_homeapp, null);

    }




    @Override
    protected void convert(BaseViewHolder baseViewHolder, App app) {

        baseViewHolder.setText(R.id.tv, app.getName());
        baseViewHolder.addOnClickListener(R.id.ll);

        try {
            Drawable icon = mContext.getPackageManager().getApplicationIcon(app.getPkg());
//            Drawable icon = mContext.getPackageManager().getApplicationBanner(app.getPkg());
            ((ImageView) baseViewHolder.getView(R.id.iv)).setImageDrawable(icon);

        } catch (PackageManager.NameNotFoundException e) {
        }
    }

}
