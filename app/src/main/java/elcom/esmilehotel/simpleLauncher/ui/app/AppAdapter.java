package elcom.esmilehotel.simpleLauncher.ui.app;

import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import elcom.esmilehotel.simpleLauncher.R;
import elcom.esmilehotel.simpleLauncher.data.model.App;

/**
 * Created by Ann on 9/28/16.
 */


public class AppAdapter extends RecyclerView.Adapter<AppAdapter.Vh> {

    private List<App> items;

    public AppAdapter() {
        this.items = new ArrayList<>();
    }

    public void addItem2(List<App> item) {
        items.clear();
        items.addAll(item);
        notifyDataSetChanged();
    }

    public void addItem(App item) {
        items.add(item);
        notifyDataSetChanged();
    }

    public App getItem(int index) {
        return items.get(index);
    }

    public List<App> getItems() {
        return items;
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }


    @Override
    public Vh onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_app, parent, false);

        return new Vh(v);
    }

    @Override
    public void onBindViewHolder(Vh holder, int position) {

        holder.setValue(items.get(position));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class Vh extends RecyclerView.ViewHolder {

        @BindView(R.id.tv)
        TextView tv;
        @BindView(R.id.iv)
        ImageView iv;

        public Vh(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.OnClick(v, getLayoutPosition());
                    }
                }
            });
        }

        public void setValue(App item) {

            tv.setText(item.getName());
            try {
                Drawable icon = itemView.getContext().getPackageManager().getApplicationIcon(item.getPkg());
                iv.setImageDrawable(icon);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                iv.setImageResource(R.mipmap.ic_launcher);
            }


        }
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public static OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void OnClick(View view, int position);
    }
}
//public class AppAdapter extends BaseQuickAdapter<App>{
//
//    public AppAdapter(){
//        super(R.layout.item_app, null);
//    }
//
//
//    @Override
//    protected void convert(BaseViewHolder baseViewHolder, App app) {
//
//        baseViewHolder.setText(R.id.tv, app.getName())
//                .addOnClickListener(R.id.ll);
//
//        try {
//            Drawable icon = mContext.getPackageManager().getApplicationIcon(app.getPkg());
//            ((ImageView) baseViewHolder.getView(R.id.iv)).setImageDrawable(icon);
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//
//
//    }
//}
