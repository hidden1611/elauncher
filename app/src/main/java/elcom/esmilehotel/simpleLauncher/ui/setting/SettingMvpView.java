package elcom.esmilehotel.simpleLauncher.ui.setting;

import java.util.List;

import elcom.esmilehotel.simpleLauncher.data.model.ImageBg;
import elcom.esmilehotel.simpleLauncher.ui.base.MvpView;

public interface SettingMvpView extends MvpView {

    void showProcess(boolean show);

    void updateData(List<ImageBg> data);
}
