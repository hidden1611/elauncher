package elcom.esmilehotel.simpleLauncher.ui.home;


import java.util.List;

import elcom.esmilehotel.simpleLauncher.data.model.App;
import elcom.esmilehotel.simpleLauncher.ui.base.MvpView;

public interface HomeMvpView extends MvpView {

    void openApp(App app);

    void updateApp(List<App> data);
}
