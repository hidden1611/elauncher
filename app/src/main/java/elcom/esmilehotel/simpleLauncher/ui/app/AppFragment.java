package elcom.esmilehotel.simpleLauncher.ui.app;

import android.animation.AnimatorInflater;
import android.animation.ValueAnimator;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import elcom.esmilehotel.simpleLauncher.R;
import elcom.esmilehotel.simpleLauncher.data.model.App;
import elcom.esmilehotel.simpleLauncher.ui.base.BaseActivity;
import elcom.esmilehotel.simpleLauncher.ui.home.HomeActivity;


public class AppFragment extends Fragment implements AppMvpView {

    @Inject
    AppPresenter mAppPresenter;

    @BindView(R.id.rv_list_allapp)
    RecyclerView rv_list;
    AppAdapter appAdapter;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_app, container, false);
        fragmentView.requestFocus();
        ButterKnife.bind(this, fragmentView);
        initRv();


        mAppPresenter.attachView(this);
        mAppPresenter.getData();





        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().requestFocus();
    }


    private void initRv() {
        rv_list.setLayoutManager(new GridLayoutManager(getActivity(), 6));
        appAdapter = new AppAdapter();
        rv_list.setAdapter(appAdapter);
        appAdapter.setOnItemClickListener(new AppAdapter.OnItemClickListener() {
            @Override
            public void OnClick(View view, int position) {
                ((HomeActivity)getActivity()).openApp(appAdapter.getItem(position));

            }
        });

        ((HomeActivity) getActivity()).startSyncService();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mAppPresenter.detachView();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void updateData(List<App> data) {

        appAdapter.addItem2(data);
        rv_list.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(rv_list.getLayoutManager().getChildCount() > 0) {
                    rv_list.getLayoutManager().getChildAt(0).requestFocus();
                }

            }
        }, 200);

    }


}