package elcom.esmilehotel.simpleLauncher.ui.showapp;


import android.content.Context;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import elcom.esmilehotel.simpleLauncher.data.DataManager;
import elcom.esmilehotel.simpleLauncher.data.model.App;
import elcom.esmilehotel.simpleLauncher.injection.ActivityContext;
import elcom.esmilehotel.simpleLauncher.ui.base.BasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ShowAppPresenter extends BasePresenter<ShowAppMvpView> {

    @Inject
    DataManager dataManager;

    @Inject
    public ShowAppPresenter(@ActivityContext Context context) {

    }

    @Override
    public void attachView(ShowAppMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }


    public void getApp(){

        if (isViewAttached()) {
            getMvpView().showLoading();
        }
        dataManager.allApps()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<App>>() {
                    @Override
                    public void onCompleted() {
                        if (isViewAttached()) {
                            getMvpView().hideLoading();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        e.printStackTrace();
                        if (isViewAttached()) {
                            getMvpView().hideLoading();
                        }

                    }

                    @Override
                    public void onNext(List<App> data) {


                        if(isViewAttached()) {
                            getMvpView().updateData(data);
                        }

                    }
                });
    }

    public void updateApps(List<App> apps){

        if (isViewAttached()) {
            getMvpView().showLoading();
        }

        dataManager.updateApps(apps)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<App>() {
                    @Override
                    public void onCompleted() {
                        if (isViewAttached()) {
                            getMvpView().hideLoading();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        e.printStackTrace();
                        if (isViewAttached()) {
                            getMvpView().hideLoading();
                        }

                    }

                    @Override
                    public void onNext(App data) {

                    }
                });
    }

    private static final String TAG = "ShowAppPresenter";
}
