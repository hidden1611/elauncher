package elcom.esmilehotel.simpleLauncher.ui.home;


import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.List;

import javax.inject.Inject;

import elcom.esmilehotel.simpleLauncher.data.BusEvent;
import elcom.esmilehotel.simpleLauncher.data.DataManager;
import elcom.esmilehotel.simpleLauncher.data.model.App;
import elcom.esmilehotel.simpleLauncher.injection.ApplicationContext;
import elcom.esmilehotel.simpleLauncher.services.KeepSessionService;
import elcom.esmilehotel.simpleLauncher.services.SyncService;
import elcom.esmilehotel.simpleLauncher.ui.base.BasePresenter;
import elcom.esmilehotel.simpleLauncher.util.AndroidComponentUtil;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class HomePresenter extends BasePresenter<HomeMvpView> {

    private final Context context;
    DataManager dataManager;
    @Inject
    Bus bus;

    @Inject
    public HomePresenter(@ApplicationContext Context context,  DataManager dataManager) {
        this.context = context;
        this.dataManager = dataManager;

    }

    @Override
    public void attachView(HomeMvpView mvpView) {
        super.attachView(mvpView);
        getShowesApps();
        bus.register(this);
    }


    @Override
    public void detachView() {
        super.detachView();
        bus.unregister(this);
    }

    public void getShowesApps(){

        dataManager.showedApp()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<App>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        e.printStackTrace();


                    }

                    @Override
                    public void onNext(List<App> data) {
                        Log.d(TAG, "onNext " + data.size());

                        if (isViewAttached()){
                            getMvpView().updateApp(data);
                        }

                    }
                });

    }

    private static final String TAG = "nna - HomePresenter";


    public void startService(){

        Intent ii = new Intent(context, SyncService.class);
        if(AndroidComponentUtil.isServiceRunning(context, SyncService.class)) {
            context.stopService(ii);
        }

        context.startService(ii);

    }



}
