package elcom.esmilehotel.simpleLauncher.ui.app;


import java.util.List;

import elcom.esmilehotel.simpleLauncher.data.model.App;
import elcom.esmilehotel.simpleLauncher.ui.base.MvpView;


public interface AppMvpView extends MvpView {

    void showLoading();

    void hideLoading();

    void updateData(List<App> data);
}
