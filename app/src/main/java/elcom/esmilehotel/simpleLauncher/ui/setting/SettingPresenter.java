package elcom.esmilehotel.simpleLauncher.ui.setting;

import java.util.List;

import javax.inject.Inject;

import elcom.esmilehotel.simpleLauncher.data.DataManager;
import elcom.esmilehotel.simpleLauncher.data.model.ImageBg;
import elcom.esmilehotel.simpleLauncher.ui.base.BasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SettingPresenter extends BasePresenter<SettingMvpView> {

    DataManager dataManager;

    @Inject
    public SettingPresenter(DataManager dataManager) {

        this.dataManager = dataManager;

    }

    @Override
    public void attachView(SettingMvpView mvpView) {
        super.attachView(mvpView);
        getBackground();
    }

    @Override
    public void detachView() {
        super.detachView();
    }


    private void getBackground(){


        dataManager.backgrounds()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<ImageBg>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        e.printStackTrace();


                    }

                    @Override
                    public void onNext(List<ImageBg> data) {

                        if(isViewAttached()){
                            getMvpView().updateData(data);
                        }
                    }
                });
    }


}

