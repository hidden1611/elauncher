package elcom.esmilehotel.simpleLauncher.ui.home;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.f2prateek.rx.receivers.RxBroadcastReceiver;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import elcom.esmilehotel.simpleLauncher.R;
import elcom.esmilehotel.simpleLauncher.data.BusEvent;
import elcom.esmilehotel.simpleLauncher.data.local.PreferencesHelper;
import elcom.esmilehotel.simpleLauncher.data.model.App;
import elcom.esmilehotel.simpleLauncher.ui.app.AppFragment;
import elcom.esmilehotel.simpleLauncher.ui.base.BaseActivity;
import elcom.esmilehotel.simpleLauncher.ui.setting.SettingFragment;
import elcom.esmilehotel.simpleLauncher.ui.showapp.ShowAppFragment;
import elcom.esmilehotel.simpleLauncher.util.NetworkUtil;
import rx.Subscriber;
import rx.Subscription;


public class HomeActivity extends BaseActivity implements HomeMvpView {

    @Inject
    HomePresenter mHomePresenter;
    @Inject
    Bus bus;
    @Inject
    PreferencesHelper preferencesHelper;

    @BindView(R.id.ivNetworkState)
    ImageView ivNetworkState;

    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.ivBg)
    ImageView ivBg;

    HomeAppAdapter homeAppAdapter;


    private Subscription subscriptionBroadcast;

    static {

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        activityComponent().inject(this);
        ButterKnife.bind(this);
        mHomePresenter.attachView(this);
        initRv();
        registerBroadcastNetwork();
        setDate();
        
        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Log.d(TAG, "onBackStackChanged ");

                try {

                    rv.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if(rv.getLayoutManager().getChildCount() > 0) {
                                rv.getLayoutManager().getChildAt(0).requestFocus();
                            }

                        }
                    }, 200);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
        bus.register(this);

        String bg = preferencesHelper.getValue(PreferencesHelper.PREF_BACKGROUND, "");
        Glide.with(this)
                .load(bg)
                .centerCrop()
                .crossFade()
                .error(R.drawable.bg)
                .placeholder(R.drawable.bg)
                .into(ivBg);


    }

    private static final String TAG = "nna - HomeActivity";

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume ");
        mHomePresenter.startService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause ");
    }

    private void setDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd - MM - yyy");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+07"));
        tvDate.setText(simpleDateFormat.format(new Date()));
    }

    private void initRv() {
        homeAppAdapter = new HomeAppAdapter();
        View emptyView = View.inflate(this, R.layout.item_empty, null);
        homeAppAdapter.setEmptyView(emptyView);
        homeAppAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                openApp(homeAppAdapter.getItem(position));

            }
        });
        homeAppAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                openApp(homeAppAdapter.getItem(position));
            }
        });
        rv.setLayoutManager(new GridLayoutManager(this,4));
        rv.setAdapter(homeAppAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHomePresenter.detachView();
        if(subscriptionBroadcast != null){
            subscriptionBroadcast.unsubscribe();
        }
        bus.unregister(this);
    }

    @OnClick({R.id.butAddIcon, R.id.butApp, R.id.butConfig, R.id.butNetwork, R.id.butBackground})
    public void OnClickedFooter(View v){

        switch (v.getId()){
            case R.id.butApp:
                showFragment(new AppFragment());
                break;
            case R.id.butAddIcon:

                showFragment(new ShowAppFragment());
                break;
            case R.id.butConfig:
                Intent ii = new Intent(Settings.ACTION_SETTINGS);
                startActivity(ii);
                break;
            case R.id.butNetwork:
                Intent ii2 = new Intent(Settings.ACTION_WIFI_SETTINGS);
                startActivity(ii2);
                break;

            case R.id.butBackground:
                showFragment(new SettingFragment());
                break;
        }
    }

    public void showFragment(Fragment fragment){
        getFragmentManager().beginTransaction()
                .replace(R.id.frame_layout, fragment)
                .addToBackStack(null)
                .commit();
    }

    //region check network
    private void registerBroadcastNetwork() {
        //region checknetwork
        int state = NetworkUtil.getTypeNetworkConnected(this);
        if(state == 0){

            ivNetworkState.setImageResource(R.drawable.ic_wifi_off);

        }else if(((state >> 4) & 0x000F) == 2) {

            ivNetworkState.setImageResource(R.drawable.ic_ethernet);

        }else if(((state >> 4 ) & 0x000F) == 1) {

            int level = state & 0x000F;
            ivNetworkState.setImageResource(R.drawable.ic_signal_wifi_bar);
            ivNetworkState.setImageLevel(level);

        }else {

            ivNetworkState.setImageResource(R.drawable.ic_wifi_off);

        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        intentFilter.addAction(WifiManager.RSSI_CHANGED_ACTION);
        subscriptionBroadcast = RxBroadcastReceiver.create(this, intentFilter)
                .subscribe(new Subscriber<Intent>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        e.printStackTrace();

                    }

                    @Override
                    public void onNext(Intent intent) {
                        checkNetworkState(intent);

                    }
                });
    }

    private void checkNetworkState(Intent intent) {

        int state = NetworkUtil.getTypeNetworkConnected(this);

        setDate();
        if(state == 0){

            ivNetworkState.setImageResource(R.drawable.ic_wifi_off);


        }else if(((state >> 4) & 0x000F) == 2) {

            ivNetworkState.setImageResource(R.drawable.ic_ethernet);

        }else if(((state >> 4 ) & 0x000F) == 1) {

            int level = state & 0x000F;
            ivNetworkState.setImageResource(R.drawable.ic_signal_wifi_bar);
            ivNetworkState.setImageLevel(level);

        }else {

            ivNetworkState.setImageResource(R.drawable.ic_wifi_off);

        }
    }
    //endregion


    @Override
    public void openApp(App app) {

        Intent launcherIntent = getPackageManager().getLaunchIntentForPackage(app.getPkg());
        startActivity(launcherIntent);

    }

    @Override
    public void updateApp(List<App> data) {
        homeAppAdapter.setNewData(data);

    }

    public void startSyncService(){
        mHomePresenter.startService();
    }


    @Subscribe
    public void eventChangeBackground(BusEvent.EventBackground eventBackground){

        Glide.with(this)
                .load(eventBackground.bg)
                .centerCrop()
                .crossFade()
                .error(R.drawable.bg)
                .placeholder(R.drawable.bg)
                .into(ivBg);

        preferencesHelper.setValue(PreferencesHelper.PREF_BACKGROUND, eventBackground.bg);

    }
}