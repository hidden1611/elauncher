package elcom.esmilehotel.simpleLauncher.ui.app;


import java.util.List;

import javax.inject.Inject;

import elcom.esmilehotel.simpleLauncher.data.DataManager;
import elcom.esmilehotel.simpleLauncher.data.model.App;
import elcom.esmilehotel.simpleLauncher.ui.base.BasePresenter;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AppPresenter extends BasePresenter<AppMvpView> {

    @Inject
    DataManager dataManager;

    @Inject
    public AppPresenter(DataManager dataManager) {

        this.dataManager = dataManager;
    }

    @Override
    public void attachView(AppMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public void getData(){

        if (isViewAttached()) {
            getMvpView().showLoading();
        }
        dataManager.allApps()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<App>>() {
                    @Override
                    public void onCompleted() {
                        if (isViewAttached()) {
                            getMvpView().hideLoading();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        e.printStackTrace();
                        if (isViewAttached()) {
                            getMvpView().hideLoading();
                        }

                    }

                    @Override
                    public void onNext(List<App> data) {

                        if(isViewAttached()){
                            getMvpView().updateData(data);
                        }

                    }
                });
    }

    private static final String TAG = "AppPresenter";


}
