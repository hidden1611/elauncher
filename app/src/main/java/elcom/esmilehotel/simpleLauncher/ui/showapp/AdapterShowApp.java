package elcom.esmilehotel.simpleLauncher.ui.showapp;

/**
 * Created by Ann on 9/27/16.
 */

import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import elcom.esmilehotel.simpleLauncher.R;
import elcom.esmilehotel.simpleLauncher.data.model.App;


public class AdapterShowApp extends RecyclerView.Adapter<AdapterShowApp.Vh> {

    private List<App> items;

    public AdapterShowApp() {
        this.items = new ArrayList<>();
    }

    public void addItem2(List<App> item) {
        items.clear();
        items.addAll(item);
        notifyDataSetChanged();
    }

    public void addItem(App item) {
        items.add(item);
        notifyDataSetChanged();
    }

    public App getItem(int index) {
        return items.get(index);
    }

    public List<App> getItems() {
        return items;
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }


    @Override
    public Vh onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_showapp, parent, false);

        return new Vh(v);
    }

    @Override
    public void onBindViewHolder(Vh holder, int position) {

        holder.setValue(items.get(position));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class Vh extends RecyclerView.ViewHolder {


        @BindView(R.id.tv)
        TextView tv;
        @BindView(R.id.iv)
        ImageView iv;
        @BindView(R.id.item_showapp)
        ConstraintLayout item_showapp;
        @BindView(R.id.ivAdd)
        ImageView ivAdd;

        public Vh(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            item_showapp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onItemClickListener != null){
                        onItemClickListener.OnClick(view, getLayoutPosition());
                    }
                }
            });
        }

        public void setValue(App item) {

            tv.setText(item.getName());
            try {
                Drawable icon = itemView.getContext().getPackageManager().getApplicationIcon(item.getPkg());
                iv.setImageDrawable(icon);
            } catch (PackageManager.NameNotFoundException e) {
//                e.printStackTrace();
                iv.setImageResource(R.mipmap.ic_launcher);
            }

            if(item.getIsPublic() != -1){

                ivAdd.setImageResource(R.drawable.ic_showapp_minus);

            }else {

                ivAdd.setImageResource(R.drawable.ic_showapp_plus);

            }


        }
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public static OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void OnClick(View view, int position);
    }
}

