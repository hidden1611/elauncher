package elcom.esmilehotel.simpleLauncher.ui.showapp;


import java.util.List;

import elcom.esmilehotel.simpleLauncher.data.model.App;
import elcom.esmilehotel.simpleLauncher.ui.base.MvpView;


public interface ShowAppMvpView extends MvpView {

    void showLoading();

    void hideLoading();

    void updateData(List<App> data);
}
