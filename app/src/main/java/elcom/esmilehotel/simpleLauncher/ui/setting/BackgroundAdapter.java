package elcom.esmilehotel.simpleLauncher.ui.setting;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import elcom.esmilehotel.simpleLauncher.R;
import elcom.esmilehotel.simpleLauncher.data.model.ImageBg;

/**
 * Created by Ann on 4/19/17.
 */

public class BackgroundAdapter extends BaseQuickAdapter<ImageBg, BaseViewHolder> {

    public BackgroundAdapter() {
        super(R.layout.item_background, null);
    }

    @Override
    protected void convert(BaseViewHolder helper, ImageBg item) {

        Glide.with(mContext)
                .load(item.getUrl())
                .centerCrop()
                .crossFade()
                .into((ImageView) helper.getView(R.id.iv));

        helper.addOnClickListener(R.id.ll);

    }
}
