package elcom.esmilehotel.simpleLauncher.injection.component;

import dagger.Component;
import elcom.esmilehotel.simpleLauncher.ui.MainActivity;
import elcom.esmilehotel.simpleLauncher.injection.PerActivity;
import elcom.esmilehotel.simpleLauncher.injection.module.ActivityModule;
import elcom.esmilehotel.simpleLauncher.ui.app.AppFragment;
import elcom.esmilehotel.simpleLauncher.ui.home.HomeActivity;
import elcom.esmilehotel.simpleLauncher.ui.setting.SettingFragment;
import elcom.esmilehotel.simpleLauncher.ui.showapp.ShowAppFragment;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

    void inject(HomeActivity homeActivity);

    void inject(AppFragment appFragment);

    void inject(ShowAppFragment showAppFragment);

    void inject(SettingFragment settingFragment);
}

