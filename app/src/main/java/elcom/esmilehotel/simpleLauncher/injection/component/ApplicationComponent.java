package elcom.esmilehotel.simpleLauncher.injection.component;

import android.app.Application;
import android.content.Context;

import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Component;
import elcom.esmilehotel.simpleLauncher.SupervisorApplication;
import elcom.esmilehotel.simpleLauncher.data.DataManager;
import elcom.esmilehotel.simpleLauncher.data.local.PreferencesHelper;
import elcom.esmilehotel.simpleLauncher.injection.ApplicationContext;
import elcom.esmilehotel.simpleLauncher.injection.module.ApplicationModule;
import elcom.esmilehotel.simpleLauncher.services.KeepSessionService;
import elcom.esmilehotel.simpleLauncher.services.SyncService;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(SupervisorApplication ikyBikeApplication);

    @ApplicationContext
    Context context();
    Application application();
    Bus eventBus();
    DataManager dataManager();
    PreferencesHelper prefereceHelper();

    void inject(KeepSessionService keepSessionService);

    void inject(SyncService syncService);
}
