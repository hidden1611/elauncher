package elcom.esmilehotel.simpleLauncher.test.common.injection.component;

import javax.inject.Singleton;

import dagger.Component;
import elcom.esmilehotel.simpleLauncher.injection.component.ApplicationComponent;
import elcom.esmilehotel.simpleLauncher.test.common.injection.module.ApplicationTestModule;

@Singleton
@Component(modules = ApplicationTestModule.class)
public interface TestComponent extends ApplicationComponent {

}
