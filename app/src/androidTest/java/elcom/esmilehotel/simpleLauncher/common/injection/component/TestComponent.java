package elcom.esmilehotel.simpleLauncher.common.injection.component;

import javax.inject.Singleton;

import dagger.Component;
import elcom.esmilehotel.simpleLauncher.common.injection.module.ApplicationTestModule;
import elcom.esmilehotel.simpleLauncher.injection.component.ApplicationComponent;

@Singleton
@Component(modules = ApplicationTestModule.class)
public interface TestComponent extends ApplicationComponent {

}
