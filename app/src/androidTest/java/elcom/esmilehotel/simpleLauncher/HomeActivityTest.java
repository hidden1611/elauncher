package elcom.esmilehotel.simpleLauncher;

import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import elcom.esmilehotel.simpleLauncher.common.TestComponentRule;
import elcom.esmilehotel.simpleLauncher.ui.home.HomeActivity;
import rx.Observable;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.mockito.Mockito.doReturn;

/**
 * Created by Ann on 10/1/16.
 */

@RunWith(AndroidJUnit4.class)
public class HomeActivityTest {


    public final TestComponentRule component =
            new TestComponentRule(InstrumentationRegistry.getTargetContext());
    public final ActivityTestRule<HomeActivity> main =
            new ActivityTestRule<HomeActivity>(HomeActivity.class, false, false);
    // TestComponentRule needs to go first so we make sure the ApplicationTestComponent is set
    // in the Application before any Activity is launched.
    @Rule
    public TestRule chain = RuleChain.outerRule(component).around(main);


    @Test
    public void signOutSuccessful() {
        doReturn(Observable.empty())
                .when(component.getMockDataManager())
                .getData();

        main.launchActivity(null);

        onView(withText("aa"))
                .perform(click());

    }

}
