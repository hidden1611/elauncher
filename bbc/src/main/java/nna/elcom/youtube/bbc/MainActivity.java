package nna.elcom.youtube.bbc;

import android.content.Intent;
import android.net.Uri;
import android.provider.UserDictionary;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String youtubePkg = "com.google.android.youtube.tv";
        Intent launcherIntent = getPackageManager().getLaunchIntentForPackage(youtubePkg);
        launcherIntent.setData(Uri.parse("https://www.youtube.com/user/BBCVietnamese"));
        startActivity(launcherIntent);
        finish();


    }
}
